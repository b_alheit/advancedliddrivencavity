//
// Created by benjamin on 2018/09/04.
//

#ifndef ADVANCEDLIDDRIVENCAVITY_MESH2D_H
#define ADVANCEDLIDDRIVENCAVITY_MESH2D_H

#include <eigen3/Eigen/Dense>
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <dirent.h>
#include <sys/stat.h>
#include <cmath>

using namespace std;
using namespace Eigen;

class Mesh2D {
public:
    Mesh2D(){};
    Mesh2D(double xMin,
           double xMax,
           double yMin,
           double yMax,
           unsigned int nXNodes,
           unsigned int nYNodes,
           double xScalingFactor,
           double yScalingFactor);

    void writeData(string simName, bool initiateSimDir);

    const unsigned long int getnNodes();
    const double getX(unsigned long int node);
    const double getY(unsigned long int node);
    const double getV(unsigned long int node);
    const double getDxEff(unsigned long int node);
    const double getXNodesPos(unsigned long int xNode);
    const double getYNodesPos(unsigned long int yNode);

    const vector<unsigned long int> getNeighbourIndicies(unsigned long int node);
    const vector<double> getNodeNodeLengths(unsigned long int node);
    const vector<double> getFaceAreas(unsigned long int node);
    const vector<Vector2d> getFaceNormals(unsigned long int node);
    const vector<unsigned long int> getN();
    const vector<unsigned long int> getS();
    const vector<unsigned long int> getE();
    const vector<unsigned long int> getW();

private:
    unsigned long int nNodes;
    ArrayXd xVerticies, yVerticies, xNodes, yNodes, x, y, V;
    vector<vector<double>> lAdj, AAdj;
    vector<vector<unsigned long int>> neighbourVec;
    vector<Vector2d> normalNSEW;
    vector<unsigned long int> N, S, E, W;
    vector<double> dxEff;

};


#endif //ADVANCEDLIDDRIVENCAVITY_MESH2D_H
