//
// Created by benjamin on 2018/09/08.
//

#ifndef GMRESSOLVER_GMRESSOLVER_H
#define GMRESSOLVER_GMRESSOLVER_H

#include <eigen3/Eigen/Sparse>
#include <eigen3/Eigen/Dense>
#include <cmath>
#include <iostream>

using namespace Eigen;
using namespace std;

class GMRESSolver {
public:
    GMRESSolver(){}
    GMRESSolver(SparseMatrix<double> A, int kryVecs);
    void diagonalPrecondition();
    ArrayXd solve(ArrayXd c, double err, VectorXd bGuess);
    void preconditionC();
    void calcRes();
    void calcE();

private:
    long unsigned int rows;
    double e, solveE;
    int kryVecs;
    VectorXd v0, w, a, b, bVec, deltaB, res;
    MatrixXd vMat, AMat;
    SparseMatrix<double> A;
    ArrayXd preCon, c;

};


#endif //GMRESSOLVER_GMRESSOLVER_H
