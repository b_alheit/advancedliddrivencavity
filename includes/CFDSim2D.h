//
// Created by cerecam on 9/5/18.
//

#ifndef ADVANCEDLIDDRIVENCAVITY_CFDSIM2D_H
#define ADVANCEDLIDDRIVENCAVITY_CFDSIM2D_H

#include "Mesh2D.h"
#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include <dirent.h>
#include <sys/stat.h>
#include <cmath>
#include <eigen3/Eigen/Sparse>
#include "GMRESSolver.h"
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/IterativeLinearSolvers>
#include <eigen3/unsupported/Eigen/IterativeSolvers>

using namespace std;
using namespace Eigen;

class CFDSim2D {
public:
    CFDSim2D(){};
    CFDSim2D(double xMin,
             double xMax,
             double yMin,
             double yMax,
             unsigned int nXNodes,
             unsigned int nYNodes,
             double xScalingFactor,
             double yScalingFactor);
    CFDSim2D(Mesh2D& inMesh);
    void assignValues(string simName, double rho, double mu, double Pref, double CFL, double err, vector<Vector2d> NSEWBCs,
                      unsigned long int writeIts, double apprxTime, bool GMRES, double kappa, bool preCondition, int kryVecs, double solverErr, bool eigenSolver,
                      unsigned long int maxSolverIterations);
    void solve();

private:
    Mesh2D mesh;
    double t, dt, rho, mu, Pref, CFL, solveErr, currentErr, dErr, writeErr, apprxTime, writeTime, dWriteTime, kappa, solverErr;
    unsigned long int it, nNodes, writeIts, maxSolverIterations;
    int kryVecs;
    bool converged, solveGMRES, preCondition, useEigenSolver;
    vector<Vector2d> u, uPrev, ws, NSEWBCs;
    VectorXd P, b, bAddjust;
    MatrixXd Pmat, Pmatinv;
    SparseMatrix<double> PSparse;
    ArrayXXd uEig, uPrevEig, wsEig, convec, diffuse, delP;
    ofstream wux, wuy, wp, wt, werr;
    string simName;

    GMRESSolver solver;
    GMRES<SparseMatrix<double>, Eigen::DiagonalPreconditioner<double>> eigenSolver;

    void applyGeneralConstructions();
    void calcDT();
    void calcWs();
    void calcB();
    void calcP();
    void constructPmat();
    void calcDelP();
    void calcU();
    void calcDerr();
    void checkConvergence();
    void applyBCs();
    void openWriters();
    void closeWriters();
    void writeCurrentData();
};


#endif //ADVANCEDLIDDRIVENCAVITY_CFDSIM2D_H
