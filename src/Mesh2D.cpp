//
// Created by benjamin on 2018/09/04.
//

#include "../includes/Mesh2D.h"

ArrayXd findStrechedArray(unsigned int nNodes, double scalingFactor, double min, double max);
ArrayXd findDNodes(ArrayXd nodes);
ArrayXd findLNodes(ArrayXd nodes);

Mesh2D::Mesh2D(double xMin, double xMax, double yMin, double yMax, unsigned int nXNodes, unsigned int nYNodes,
               double xScalingFactor, double yScalingFactor) {
    cout << "Constructing mesh" << endl;

    unsigned long int currentNode = 0;

    cout << "\tInitiating basic arrays" << endl;
    normalNSEW = {Vector2d{0, 1},
                  Vector2d{0, -1},
                  Vector2d{1, 0},
                  Vector2d{-1, 0},
                  };

    nNodes = nXNodes * nYNodes;

    x = ArrayXd::Zero(nNodes);
    y = ArrayXd::Zero(nNodes);
    V = ArrayXd::Zero(nNodes);

    cout << "\tConstructing arrays of repeated values" << endl;
    xNodes = findStrechedArray(nXNodes, xScalingFactor, xMin, xMax);
    yNodes = findStrechedArray(nYNodes, yScalingFactor, yMin, yMax);

    ArrayXd dxNodes = findDNodes(xNodes);
    ArrayXd dyNodes = findDNodes(yNodes);

    ArrayXd lxNodes = findLNodes(xNodes);
    ArrayXd lyNodes = findLNodes(yNodes);

    cout << "\tAssigning values to nodes" << endl;
    for(int i = 0; i < nXNodes; i++){
        for(int j = 0; j < nYNodes; j++){
            x[currentNode] = xNodes[i];
            y[currentNode] = yNodes[j];

            V[currentNode] = dxNodes[i] * dyNodes[j];
            neighbourVec.push_back(vector<unsigned long int> {currentNode+1, currentNode-1, currentNode+nYNodes, currentNode-nYNodes});
            lAdj.push_back(vector<double> {lyNodes[j+1], lyNodes[j], lxNodes[i+1], lxNodes[i]});
            AAdj.push_back(vector<double> {dxNodes[i], dxNodes[i], dyNodes[j], dyNodes[j]});


            if(i==0){
                W.push_back(currentNode);
                neighbourVec.at(currentNode).at(3) = nNodes+1;
            }
            if(i==nXNodes-1){
                E.push_back(currentNode);
                neighbourVec.at(currentNode).at(2) = nNodes+1;
                }
            if(j == 0){
                S.push_back(currentNode);
                neighbourVec.at(currentNode).at(1) = nNodes+1;
                }
            if(j==nYNodes-1){
                N.push_back(currentNode);
                neighbourVec.at(currentNode).at(0) = nNodes+1;
            }
            dxEff.push_back(pow(pow(dxNodes[i],-2) + pow(dyNodes[j], -2), -0.5));
            currentNode++;
        }
    }
    cout << "Mesh constructed" << endl;
}

ArrayXd findStrechedArray(unsigned int nNodes, double scalingFactor, double min, double max){
    ArrayXd nodePositions(nNodes);
    double l = max - min;

    ArrayXd firstHalf((nNodes + 1)/2);
    ArrayXd secondHalf((nNodes)/2);

    firstHalf[0] = 0;
    firstHalf[1] = 1;


    for(int i = 2; i < (nNodes + 1)/2; i++){
        firstHalf[i] = firstHalf[i-1] + scalingFactor * (firstHalf[i-1] - firstHalf[i-2]);
    }
    firstHalf = firstHalf * (l/2) / firstHalf[(nNodes + 1)/2-1] - (l/2);
    secondHalf = -1*firstHalf.segment(0, (nNodes + 1)/2-1).reverse();

    nodePositions << firstHalf, secondHalf;
    nodePositions += min - nodePositions[0];
    return nodePositions;
}

ArrayXd findDNodes(ArrayXd nodes){
    unsigned int nNodes = nodes.size();
    ArrayXd dNodes(nNodes);

    dNodes[0] = (nodes[1] - nodes[0])/2;
    dNodes[nNodes-1] = (nodes[nNodes-1] - nodes[nNodes-2])/2;

    for(int i = 1; i < nNodes - 1; i++){
        dNodes[i] = (nodes[i+1] - nodes[i-1])/2;
    }

    return dNodes;
}

ArrayXd findLNodes(ArrayXd nodes){
    unsigned int nNodes = nodes.size();
    ArrayXd lNodes(nNodes+1);
    lNodes[0] = NAN;

    for(int i = 1; i < nNodes; i++){
        lNodes[i] = nodes[i] - nodes[i-1];
    }
    lNodes[nNodes] = NAN;

    return lNodes;
}

void Mesh2D::writeData(string simName, bool initiateSimDir) {
    string simPath = "../results/" + simName;
    if(initiateSimDir) mkdir(simPath.c_str(), 0777);
    string meshPath = simPath +"/mesh";
    mkdir(meshPath.c_str(), 0777);
    ofstream wx, wy, wxNodes, wyNodes;
    wx.open(meshPath + "/x.bin", ios::binary);
    wy.open(meshPath + "/y.bin", ios::binary);
    wxNodes.open(meshPath + "/xNodes.bin", ios::binary);
    wyNodes.open(meshPath + "/yNodes.bin", ios::binary);

    for (int i=0; i < x.size(); i++){
        wx.write(reinterpret_cast<const char*>(&x[i]), sizeof(double));
    }

    for(int i=0; i < y.size(); i++)
        wy.write(reinterpret_cast<const char*>(&y[i]), sizeof(double));

    for(int i = 0; i < xNodes.size(); i++){
        wxNodes.write(reinterpret_cast<const char*>(&xNodes[i]), sizeof(double));
    }

    for(int i=0; i < yNodes.size(); i++)
        wyNodes.write(reinterpret_cast<const char*>(&yNodes[i]), sizeof(double));


    wx.close();
    wy.close();
    wxNodes.close();
    wyNodes.close();
}


const unsigned long int Mesh2D::getnNodes() {return nNodes;}

const double Mesh2D::getX(unsigned long int node){return x[node];}
const double Mesh2D::getY(unsigned long int node){return y[node];}
const double Mesh2D::getV(unsigned long int node){return V[node];}
const double Mesh2D::getDxEff(unsigned long int node){ return dxEff.at(node);}
const double Mesh2D::getXNodesPos(unsigned long int xNode){return xNodes[xNode];}
const double Mesh2D::getYNodesPos(unsigned long int yNode){return yNodes[yNode];}

const vector<unsigned long int> Mesh2D::getNeighbourIndicies(unsigned long int node) {return neighbourVec.at(node);}
const vector<double> Mesh2D::getNodeNodeLengths(unsigned long int node){return lAdj.at(node);}
const vector<double> Mesh2D::getFaceAreas(unsigned long int node){return AAdj.at(node);}
const vector<Vector2d> Mesh2D::getFaceNormals(unsigned long int node){return normalNSEW;}

const vector<unsigned long int> Mesh2D::getN(){return N;}
const vector<unsigned long int> Mesh2D::getS(){return S;}
const vector<unsigned long int> Mesh2D::getE(){return E;}
const vector<unsigned long int> Mesh2D::getW(){return W;}