//
// Created by benjamin on 2018/09/08.
//

#include "../includes/GMRESSolver.h"


GMRESSolver::GMRESSolver(SparseMatrix<double> A, int kryVecs) {
    this->A = A;
    this->kryVecs = kryVecs;
    this->rows = A.rows();
    this->preCon = ArrayXd::Ones(rows);
    this->b = VectorXd::Zero(rows);
    this->deltaB = VectorXd::Zero(rows);

    this->v0 = VectorXd::Zero(rows);
    this->w = VectorXd::Zero(rows);
    this->a = VectorXd::Zero(kryVecs);
    this->bVec = VectorXd::Zero(kryVecs);
    this->vMat = MatrixXd::Zero(rows, kryVecs);
    this->AMat = MatrixXd::Zero(kryVecs, kryVecs);
}

void GMRESSolver::diagonalPrecondition() {
    for(long unsigned int i = 0; i < rows; i++){
        preCon(i) = 1/A.coeff(i,i);
        A.coeffRef(i,i) = 1;
    }
}

void GMRESSolver::preconditionC() {
    c *= preCon;
}

ArrayXd GMRESSolver::solve(ArrayXd solveArray, double err, VectorXd bGuess) {
    this->c = solveArray;
    this->b = bGuess;
    preconditionC();
    this->solveE = err;

    calcRes();
    calcE();
    while(e > solveE){

        v0 = res;
        vMat.col(0) = v0.normalized();

        for(int j = 0; j < kryVecs-1; j++){
            w = A * vMat.col(j);

            for(int i = 0; i <= j; i++){
                w -=  (vMat.col(i).dot(A * vMat.col(j))) * vMat.col(i);
            }
            vMat.col(j+1) = w.normalized();
        }


        for(int k = 0; k < kryVecs; k++){

            bVec(k) = (A * vMat.col(k)).dot(res);
            for(int l = 0; l < kryVecs; l++){
                AMat(k,l) = (A * vMat.col(k)).dot(A * vMat.col(l));
            }
        }
        a = AMat.inverse() * bVec;

        deltaB = vMat * a;
        b += deltaB;
        calcRes();
        calcE();
//        cout << "e: " << e << endl;
    }
//    cout << "res: " << res.transpose() << endl;
//    cout << "A: " << endl << A << endl;
//    cout << "c: " << c.transpose() << endl;
//    cout << "b: " << b.transpose() << endl;
    return b;
}

void GMRESSolver::calcRes() {res = ((VectorXd) c) - (A*b);}

void GMRESSolver::calcE() {
    e = res.norm() / sqrt(rows);
}