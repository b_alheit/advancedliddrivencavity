//
// Created by cerecam on 9/5/18.
//

#include "../includes/CFDSim2D.h"

CFDSim2D::CFDSim2D(double xMin, double xMax, double yMin, double yMax, unsigned int nXNodes, unsigned int nYNodes,
                   double xScalingFactor, double yScalingFactor):mesh(xMin, xMax, yMin, yMax, nXNodes, nYNodes, xScalingFactor, yScalingFactor){
    applyGeneralConstructions();
}

CFDSim2D::CFDSim2D(Mesh2D& inMesh){
    mesh = inMesh;
    applyGeneralConstructions();
}

void CFDSim2D::applyGeneralConstructions() {
    cout << "Constructing simulation" << endl;

    nNodes = mesh.getnNodes();
    u = vector<Vector2d>(nNodes);
    uPrev = vector<Vector2d>(nNodes);
    ws = vector<Vector2d>(nNodes);
    uEig = ArrayXXd::Zero(nNodes, 2);
    uPrevEig = ArrayXXd::Zero(nNodes, 2);
    wsEig = ArrayXXd::Zero(nNodes, 2);
    delP = ArrayXXd::Zero(nNodes, 2);
    t = 0;
    it = 0;
    dErr = 0;
    writeErr = 0;
    converged = false;


    cout << "Simulation constructed" << endl << endl;
}

void CFDSim2D::assignValues(string simName, double rho, double mu, double Pref, double CFL, double err, vector<Vector2d> NSEWBCs, unsigned long int writeIts,
                            double apprxTime, bool GMRES, double kappa, bool preCondition, int kryVecs, double solverErr, bool eigenSolver, unsigned long int maxSolverIterations) {

    cout << "*******" << simName << "*******" << endl;
    this->rho = rho;
    this->mu = mu;
    this->Pref = Pref;
    this->CFL = CFL;
    this->NSEWBCs = NSEWBCs;
    this->solveErr = err;
    this->simName = simName;
    this->writeIts = writeIts;
    this->writeTime = 0;
    this->dWriteTime = apprxTime / writeIts;
    this->solveGMRES = GMRES;
    this->preCondition = preCondition;
    this->kappa = kappa;
    this->solverErr = solverErr;
    this->kryVecs = kryVecs;
    this->useEigenSolver = eigenSolver;
    this->maxSolverIterations = maxSolverIterations;

    constructPmat();
}

void CFDSim2D::solve() {
    applyBCs();
    openWriters();
    while(!converged){
        calcDT();
        t += dt;
        uPrevEig = uEig;



        calcWs();
        calcP();

        calcU();



        applyBCs();

        if(it%1000 ==0) {
            cout << "Iteration: " << it << endl;
            checkConvergence();
        }
//        if(it >= 4){
//            if(it == 1000){
//                calcDerr();
//                writeErr = currentErr - dErr;
//            }
//            if(currentErr < writeErr){
//                cout << "Writing data..." << endl;
//                writeCurrentData();
//                writeErr = currentErr - dErr;
//            }
//
//        }else if(it % 100 == 0 || it == 0)
        if(t > writeTime) {
            checkConvergence();
            writeCurrentData();
            writeTime += dWriteTime;
        }

        it++;
    }
    writeCurrentData();
    closeWriters();
}

void CFDSim2D::calcWs() {
    Vector2d u, uCd, uUp, uNeighbour, uU, uD, uS, uC, dudx;
    double xc, xu, xd;
    int j, k, map;
    bool convec;
    wsEig *= 0;
    for(unsigned int i = 0; i < nNodes; i++){
        u = uPrevEig.row(i);

        const vector<Vector2d>& n = mesh.getFaceNormals(i);
        const vector<unsigned long int>& NSEW = mesh.getNeighbourIndicies(i);
        const double& V = mesh.getV(i);
        const vector<double>& A = mesh.getFaceAreas(i);
        const vector<double>& L = mesh.getNodeNodeLengths(i);
        for(j = 0; j<2; j++){
            convec = NSEW[0] <= nNodes &&
                     NSEW[1] <= nNodes &&
                     NSEW[2] <= nNodes &&
                     NSEW[3] <= nNodes;

            for(k = 0; k<4; k++){
                if(k%2 == 0) map = 1;
                else map = -1;

                if(NSEW.at(k) < nNodes){
                    uNeighbour = (Vector2d) uEig.row(NSEW[k]);
                    uNeighbour = uEig.row(NSEW[k]);
                    uCd = 0.5 * (u + uNeighbour);
                    if (convec) {
                        if (uCd.dot(n.at(k)) >= 0){
                            uD = uNeighbour;
                            uC = u;
//                            uU = (Vector2d) uEig.row(NSEW[k+map]);
                            if(k < 2) {
                                xd = mesh.getY(NSEW[k]);
                                xc = mesh.getY(i);
//                                xu = mesh.getY(NSEW[k+map]);
                            }
                            else {
                                xd = mesh.getX(NSEW[k]);
                                xc = mesh.getX(i);
//                                xu = mesh.getX(NSEW[k+map]);
                            }
                            dudx = (uC-uD) / (xc-xd);
//                            cout << "uC-uD: " << (uC-uD).transpose() << endl;
//                            cout << "xc-xd: " << xc-xd <<  endl;
                            uS = uD - 2 * dudx * (xd-xc);
                            uUp = uC + ((1+kappa)/4) * (uD-uC) + ((1-kappa)/4) * (uC-uS);

                        }else {
                            uU = uNeighbour;
                            uC = u;
//                            uU = (Vector2d) uEig.row(NSEW[k+map]);
                            if(k < 2) {
                                xu = mesh.getY(NSEW[k]);
                                xc = mesh.getY(i);
//                                xu = mesh.getY(NSEW[k+map]);
                            }
                            else {
                                xu = mesh.getX(NSEW[k]);
                                xc = mesh.getX(i);
//                                xu = mesh.getX(NSEW[k+map]);
                            }
                            dudx = (uC-uU) / (xc-xu);
//                            cout << "uC-uU: " << (uC-uU).transpose() << endl;
//                            cout << "xc-xu: " << xc-xu <<  endl;
                            uS = uC - 2 * dudx * (xc - xu);
                            uUp = uU + ((1+kappa)/4) * (uC-uU) + ((1-kappa)/4) * (uU-uS);
                        }
//                        cout << "uD: " << uD.transpose() << endl;
//                        cout << "uC: " << uC.transpose() << endl;
//                        cout << "uU: " << uU.transpose() << endl;
//                        cout << "uS: " << uS.transpose() << endl;
//                        cout << "uUp: " << uUp.transpose() << endl;
//                        cout << "dudx: " << dudx.transpose() << endl;
//                        cout << endl;
//                        double a = 5;
//
//                        if (uCd.dot(n.at(k)) >= 0) uUp = u;
//                        else uUp = uNeighbour;
                    } else uUp = {0, 0};

                    wsEig(i, j) += (A[k] / V) * (mu * ((uNeighbour[j] - u[j]) / L[k]) - (rho * uUp[j] * uCd.dot(n[k])));
                }
            }
        }
    }
    wsEig *= dt;
}

void CFDSim2D::calcDT() {
    double uDiff, dti;
    for(unsigned long int i=0; i<nNodes; i++){
        uDiff = u.at(i).norm() + mu / (rho * 0.45 * mesh.getDxEff(i));
        dti = mesh.getDxEff(i) / uDiff;
        if(i==0 || dti < dt) dt = dti;
    }
    dt *= CFL;
}

void CFDSim2D::constructPmat() {
    cout << "Constructing Pressure Matrix" << endl;
    int j;
    double sum;
    b = VectorXd::Zero(nNodes);
    P = Pref * VectorXd::Ones(nNodes);
    bAddjust= VectorXd::Zero(nNodes);
    if(solveGMRES){
        PSparse = SparseMatrix<double> (nNodes,nNodes);
        for (unsigned long int i = 1; i < nNodes; i++) {
            sum = 0;
            const vector<unsigned long int> &NSEW = mesh.getNeighbourIndicies(i);
            const vector<double> &A = mesh.getFaceAreas(i);
            const vector<double> &L = mesh.getNodeNodeLengths(i);
            for (j = 0; j < 4; j++) {
                if (NSEW.at(j) < nNodes + 1) {
                    sum -= A.at(j) / L.at(j);
                }
            }
            for (j = -1; j < 4; j++) {
                if (j == -1) PSparse.coeffRef(i, i) = sum;
                else if (NSEW.at(j) < nNodes) PSparse.coeffRef(i, NSEW.at(j)) = A.at(j) / L.at(j);
            }
        }
//
//        cout << "((MatrixXd)PSparse - ((MatrixXd)PSparse).transpose()): " << endl << ((MatrixXd)PSparse - ((MatrixXd)PSparse).transpose()) << endl << endl;
        bAddjust = Pref*PSparse.col(0);
        PSparse.col(0) *= 0;
        PSparse.coeffRef(0,0) = 1;
//        PSparse = ((MatrixXd) PSparse).sparseView(1e-20, 1);

//        cout << "((MatrixXd)PSparse - ((MatrixXd)PSparse).transpose()).norm(): " << endl << ((MatrixXd)PSparse - ((MatrixXd)PSparse).transpose()).norm() << endl << endl;
//        cout << "bAddjust.transpose() : " << endl << bAddjust.transpose() << endl << endl;
//        cout << "Psparse: " << endl << PSparse << endl << endl;
//        cout << "Psparse.row(0).sum(): " << endl << PSparse.row(0).sum() << endl << endl;
//        cout << "(PSparse.transpose() - PSparse).sum(): " << endl << PSparse.transpose() - PSparse << endl << endl;
        cout << "Pressure Matrix constructed" << endl;
        if(useEigenSolver){
            eigenSolver.setMaxIterations(maxSolverIterations);
            eigenSolver.setTolerance(solverErr);
            cout << "Constructing GMRES Solver" << endl;
            eigenSolver.compute(PSparse);
            cout << "GMRES Solver constructed" << endl;
        }
        else {
            cout << "Constructing GMRES Solver" << endl;
            solver = GMRESSolver(PSparse, kryVecs);
            cout << "GMRES Solver constructed" << endl;

            if (preCondition) {
                cout << "Preconditioning solver" << endl;
                solver.diagonalPrecondition();
                cout << "Solver preconditioned " << endl;
            }
        }

    }else {

        Pmat = MatrixXd::Zero(nNodes, nNodes);
        Pmatinv = MatrixXd::Zero(nNodes, nNodes);
        for (unsigned long int i = 1; i < nNodes; i++) {
            sum = 0;
            const vector<unsigned long int> &NSEW = mesh.getNeighbourIndicies(i);
            const vector<double> &A = mesh.getFaceAreas(i);
            const vector<double> &L = mesh.getNodeNodeLengths(i);
            for (j = 0; j < 4; j++) {
                if (NSEW.at(j) < nNodes + 1) {
                    sum -= A.at(j) / L.at(j);
                }
            }
            for (j = -1; j < 4; j++) {
                if (j == -1) Pmat(i, i) = sum;
                else if (NSEW.at(j) < nNodes) Pmat(i, NSEW.at(j)) = A.at(j) / L.at(j);
            }
        }
        Pmat.row(0) *= 0;
        bAddjust = Pref*Pmat.col(0);
        Pmat.col(0) *= 0;


        Pmat(0, 0) = 1;

//        cout << "(Pmat - Pmat.transpose()).sum()" << endl << (Pmat - Pmat.transpose()) << endl << endl;

//    if(solveGMRES) {
//        cout << "Dense Pressure Matrix constructed" << endl;
//        pSparse = Pmat.sparseView(Pmat(1,1), 2 * abs(abs(Pmat(1,1)) - abs(Pmat(1,0))));
//        cout << "Sparse Pressure Matrix constructed" << endl;
////        Pmat.delete();
////        cout << "Sparse Pressure Matrix constructed" << endl;
//        gmres.compute(pSparse);
//    }else {
//        cout << "Pressure Matrix constructed" << endl;
//        cout << "Inverting Pressure Matrix" << endl;
//        Pmatinv = Pmat.inverse();
//        cout << "Pressure Matrix inverted" << endl;
//    }

        cout << "Pressure Matrix constructed" << endl;
        cout << "Inverting Pressure Matrix" << endl;
        Pmatinv = Pmat.inverse();
        cout << "Pressure Matrix inverted" << endl;
    }
}

void CFDSim2D::calcB() {
    int k;
    Vector2d uCd, wsCd;
    b *= 0;
    for(unsigned long int i = 1; i < nNodes; i++){
        const vector<unsigned long int>& NSEW = mesh.getNeighbourIndicies(i);
        const vector<double>& A = mesh.getFaceAreas(i);
        const vector<double>& L = mesh.getNodeNodeLengths(i);
        const vector<Vector2d>& n = mesh.getFaceNormals(i);
        for(k = 0; k<4; k++){
            if(NSEW.at(k) <= nNodes){
                uCd = 0.5*(uEig.row(i) + uEig.row(NSEW.at(k)));
                wsCd = 0.5*(wsEig.row(i) + wsEig.row(NSEW.at(k)));
//                cout << "i =" << i << "\tk ="<<k << endl;
//                cout << "n\t\t" << n.at(k).transpose() << endl;
//                cout << "uCd\t\t" <<uCd.transpose() << endl;
//                cout << "wsCd\t" <<wsCd.transpose() << endl;
//                cout << "rho\t\t" << rho << endl;
//                cout << "A\t\t" <<A.at(k) << endl;
//                cout << endl << endl;
                b(i) += A.at(k) * (uCd+ wsCd/rho).dot(n.at(k));

            }
        }
    }
//    cout << "b" << endl << b.transpose() << endl << endl;
    b *= rho/dt;
    b -= bAddjust;
    b(0) = Pref;

    /*
     *  self.b[:] = 0
        for i in range(self.n_nodes):
            for k in range(4):
                if self.mesh.NSEW[i, k] is not None:
                    u_cd = 0.5*(self.u[i, :] + self.u[self.mesh.NSEW[i, k], :])
                    ws_cd = 0.5*(self.ws[i, :] + self.ws[self.mesh.NSEW[i, k], :])
                    self.b[i] += self.mesh.A_NSEW[i, k] * np.dot(u_cd+(1/self.rho)*ws_cd, self.mesh.n_NSEW[i, k])
        self.b *= self.rho/self.dt
        self.b[0] = self.p_ref
     * */
}

void CFDSim2D::calcP() {
    calcB();

    if(solveGMRES){
        if(useEigenSolver){
//            eigenSolver.compute(PSparse);
//            P = eigenSolver.solve(b);

            if(it == 0){
                P = eigenSolver.solve(b);
                P(0) = Pref;

            }else {
                P = eigenSolver.solveWithGuess(b, P);
                P(0) = Pref;

//            cout << "Res norm: " << (PSparse*P - b).norm() << endl;
            }
//            cout << "GMRES:    #iterations: " << eigenSolver.iterations() << "GMRES Error: " << eigenSolver.error() << endl;
//            eigenSolver.compute(PSparse);
        }else {

            P = solver.solve(b, solverErr, P);
            P(0) = Pref;
        }
    }else {

        P = Pmatinv * b;
    }
}

void CFDSim2D::calcDelP() {
    double pCd;
    int k, j;
    delP *= 0;

    for(unsigned long int i = 0; i < nNodes; i++){
        const vector<Vector2d>& n = mesh.getFaceNormals(i);
        const vector<unsigned long int>& NSEW = mesh.getNeighbourIndicies(i);
        const double& V = mesh.getV(i);
        const vector<double>& A = mesh.getFaceAreas(i);

        for (k = 0; k < 4; k++) {
            if (NSEW.at(k) < nNodes) pCd = 0.5 * (P(i) + P(NSEW.at(k)));
            else pCd = P(i);

            delP.row(i) = (Vector2d) delP.row(i) + (A.at(k) / V) * pCd * n.at(k);
        }
    }
//    cout << "delP" << endl << delP.transpose() << endl << endl;
}

void CFDSim2D::calcU() {
    calcDelP();
    uEig += (wsEig - dt * delP) / rho;
}

void CFDSim2D::calcDerr() {
    dErr = (currentErr - solveErr) / writeIts;
}

void CFDSim2D::applyBCs() {
    unsigned long int len;

    const vector<unsigned long int>& N = mesh.getN();
    len = N.size();
    for(unsigned long int i = 0; i < len; i++){
        uEig.row(N.at(i)) = NSEWBCs.at(0);
    }

    const vector<unsigned long int>& S = mesh.getS();
    len = S.size();
    for(unsigned long int i = 0; i < len; i++){
        uEig.row(S.at(i)) = NSEWBCs.at(1);
    }

    const vector<unsigned long int>& E = mesh.getE();
    len = E.size();
    for(unsigned long int i = 0; i < len; i++){
        uEig.row(E.at(i)) = NSEWBCs.at(2);
    }

    const vector<unsigned long int>& W = mesh.getW();
    len = W.size();
    for(unsigned long int i = 0; i < len; i++){
        uEig.row(W.at(i)) = NSEWBCs.at(3);
    }

}

void CFDSim2D::checkConvergence() {
    currentErr = sqrt(((uEig - uPrevEig).pow(2).sum())/ nNodes);
    cout << 100 * ((currentErr - solveErr)/ currentErr) << "% from convergence." << endl;
    cout << "t =" << t << "s" << endl <<endl;
    converged = currentErr < solveErr;
}

void CFDSim2D::openWriters() {
    mesh.writeData(simName, true);
    string simWritePath = "../results/" + simName + "/simData";
    mkdir(simWritePath.c_str(), 0777);

    wux.open(simWritePath + "/ux.bin", ios::binary);
    wuy.open(simWritePath + "/uy.bin", ios::binary);
    wp.open(simWritePath + "/p.bin", ios::binary);
    wt.open(simWritePath + "/t.bin", ios::binary);
    werr.open(simWritePath + "/err.bin", ios::binary);
}

void CFDSim2D::writeCurrentData() {
    for(long unsigned int i = 0; i < nNodes; i++){
        wux.write(reinterpret_cast<const char*>(&uEig(i,0)), sizeof(double));
        wuy.write(reinterpret_cast<const char*>(&uEig(i,1)), sizeof(double));
        wp.write(reinterpret_cast<const char*>(&P(i)), sizeof(double));
    }
    wt.write(reinterpret_cast<const char*>(&t), sizeof(double));
    werr.write(reinterpret_cast<const char*>(&currentErr), sizeof(double));
}

void CFDSim2D::closeWriters() {
    wux.close();
    wuy.close();
    wp.close();
    wt.close();
    werr.close();
}