#include <iostream>
#include "../includes/Mesh2D.h"
#include "../includes/CFDSim2D.h"
#include <eigen3/Eigen/Dense>

using namespace std;
using namespace Eigen;

string simName = "81_1.1_10";

unsigned long int iterationWrites = 100;
double apprxTime = 15;
double rho = 1;
double mu = 0.01;
double Pref = 1e5;
double CFL = 0.4;
double err = 2e-9;
double kappa = 1.0/3;
bool solveGMRES = false;
bool eigenSolver = false;
bool preCondition = false;
int kryVecs = 20;
double solverErr = 1e-5;
int maxSolverIterations = 101*101 * 2;

vector<Vector2d> NSEWBCs = {Vector2d{10, 0},
                            Vector2d{0, 0},
                            Vector2d{0, 0},
                            Vector2d{0, 0}};

double xMin = -0.5;
double xMax = 0.5;
double yMin = -0.5;
double yMax = 0.5;
unsigned int nXNodes = 81;
unsigned int nYNodes = 81;
double xScalingFactor = 1.1;
double yScalingFactor = 1.1;


int main() {

    Mesh2D myMesh(xMin, xMax, yMin, yMax, nXNodes, nYNodes, xScalingFactor, yScalingFactor);

    CFDSim2D sim(myMesh);
    sim.assignValues(simName, rho, mu, Pref, CFL, err, NSEWBCs, iterationWrites, apprxTime, solveGMRES, kappa, preCondition, kryVecs, solverErr, eigenSolver, maxSolverIterations);
    sim.solve();

    return 0;
}
