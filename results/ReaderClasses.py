import numpy as np

class MeshReader:
    def __init__(self, simName):
        self.x = np.fromfile("./" + simName + "/mesh/x.bin", dtype=np.float, count=-1)
        self.y = np.fromfile("./" + simName + "/mesh/y.bin", dtype=np.float, count=-1)
        self.xNodes = np.fromfile("./" + simName + "/mesh/xNodes.bin", dtype=np.float, count=-1)
        self.yNodes = np.fromfile("./" + simName + "/mesh/yNodes.bin", dtype=np.float, count=-1)

        self.xNodesVerticies = (self.xNodes[:-1] + self.xNodes[1:]) / 2
        self.xNodesVerticies = np.append(np.insert(self.xNodesVerticies, 0, self.xNodes[0]), self.xNodes[-1])

        self.yNodesVerticies = (self.yNodes[:-1] + self.yNodes[1:]) / 2
        self.yNodesVerticies = np.append(np.insert(self.yNodesVerticies, 0, self.yNodes[0]), self.yNodes[-1])

class SimReader:
    def __init__(self, simName):
        self.mesh = MeshReader(simName)

        self.x = self.mesh.x
        self.y = self.mesh.y
        self.xNodes = self.mesh.xNodes
        self.yNodes = self.mesh.yNodes
        self.xNodesVerticies = self.mesh.xNodesVerticies
        self.yNodesVerticies = self.mesh.yNodesVerticies

        self.lx = self.xNodesVerticies[1:] - self.xNodesVerticies[:-1]

        self.ux = np.fromfile("./" + simName + "/simData/ux.bin", dtype=np.float, count=-1)
        self.uy = np.fromfile("./" + simName + "/simData/uy.bin", dtype=np.float, count=-1)
        self.t = np.fromfile("./" + simName + "/simData/t.bin", dtype=np.float, count=-1)
        self.err = np.fromfile("./" + simName + "/simData/err.bin", dtype=np.float, count=-1)
        self.p = np.fromfile("./" + simName + "/simData/p.bin", dtype=np.float, count=-1)


        self.timeSteps = np.alen(self.t)
        self.nNodes = np.alen(self.mesh.x)

        self.uLid = []
        self.uLid1 = []

        for i in range(self.nNodes):
            if(self.y[-(i+1)] == self.yNodes[-1]):
                self.uLid.append(self.ux[-(i+1)])
            elif(self.y[-(i+1)] == self.yNodes[-2]):
                self.uLid1.append(self.ux[-(i+1)])

        self.uLid = np.array(self.uLid)
        self.uLid1 = np.array(self.uLid1)

        ltotal = self.lx.sum()
        ytop = self.yNodes[-1]
        ytop1 = self.yNodes[-2]
        ydiff = ytop - ytop1
        uDiff = self.uLid - self.uLid1

        self.Nodes_neg1 = (1 / self.nNodes)

        self.shear = 0.01*np.multiply((self.uLid - self.uLid1) / (self.yNodes[-1] - self.yNodes[-2]), self.lx).sum()

        self.uxArrange = self.ux.reshape([self.timeSteps, self.nNodes])
        self.uyArrange = self.uy.reshape([self.timeSteps, self.nNodes])
        self.pArrange = self.p.reshape([self.timeSteps, self.nNodes])

        self.uMagArrange = np.power(np.power(self.uxArrange, 2) + np.power(self.uyArrange, 2), 0.5)

        self.uxUnit = np.divide(self.uxArrange, self.uMagArrange)
        self.uyUnit = np.divide(self.uyArrange, self.uMagArrange)

        np.nan_to_num(self.uxUnit, False)
        np.nan_to_num(self.uyUnit, False)

        self.uMagPlot = np.zeros([self.timeSteps, np.alen(self.yNodes), np.alen(self.xNodes)])
        self.uxUnitPlot = np.zeros([self.timeSteps, np.alen(self.yNodes), np.alen(self.xNodes)])
        self.uyUnitPlot = np.zeros([self.timeSteps, np.alen(self.yNodes), np.alen(self.xNodes)])

        for i in range(self.timeSteps):
            self.uMagPlot[i, :, :] = np.reshape(self.uMagArrange[i], [np.alen(self.xNodes), np.alen(self.yNodes)]).transpose()
            self.uxUnitPlot[i, :, :] = np.reshape(self.uxUnit[i], [np.alen(self.xNodes), np.alen(self.yNodes)]).transpose()
            self.uyUnitPlot[i, :, :] = np.reshape(self.uyUnit[i], [np.alen(self.xNodes), np.alen(self.yNodes)]).transpose()