from results.ReaderClasses import MeshReader as mr
import matplotlib.pyplot as plt

simName = "meshGenTest3"

mesh = mr(simName)

plt.hlines(mesh.yNodesVerticies, mesh.xNodesVerticies[0], mesh.xNodesVerticies[-1])
plt.vlines(mesh.xNodesVerticies, mesh.yNodesVerticies[0], mesh.yNodesVerticies[-1])
plt.scatter(mesh.x, mesh.y)
plt.show()