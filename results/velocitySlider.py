from results.ReaderClasses import MeshReader as mr
from results.ReaderClasses import SimReader as sr
from matplotlib.widgets import Slider
import matplotlib.pyplot as plt
import numpy as np

# simName = "101x101Nodes_1.0scale"
simName = "81_1.1_10"
scale = 1

startX = 1 - scale
endX = 1

startY = 0
endY = scale

plotPlace = 0
sim = sr(simName)
nx = np.alen(sim.xNodes)
ny = np.alen(sim.yNodes)

Xv, Yv = np.meshgrid(sim.xNodesVerticies[int(nx * startX):int(nx * endX) + 2], sim.yNodesVerticies[int(ny * startY):int(ny * endY) + 2])
Xn, Yn = np.meshgrid(sim.xNodes[int(nx * startX):int(nx * endX) + 1], sim.yNodes[int(ny * startY):int(ny * endY) + 1])

fig, ax = plt.subplots(1, 1)
plt.subplots_adjust(left=0.15, bottom=0.15)
print("Time steps: ", sim.timeSteps)
Col = ax.pcolor(Xv, Yv, sim.uMagPlot[plotPlace, int(ny * startY):int(ny * endY) + 1, int(nx * startX):int(nx * endX) + 1]
                , vmin=0, vmax=np.max(sim.uMagPlot[-1, int(ny * startY):int(ny * endY) + 1, int(nx * startX):int(nx * endX) + 1]), cmap='jet')
# Col = ax.pcolor(Xv, Yv, sim.uMagPlot[plotPlace, int(nx * startX):int(nx * endX) + 1, int(ny * startY):int(ny * endY) + 1]
#                 , vmin=0, vmax=np.max(sim.uMagPlot[-1, int(nx * startX):int(nx * endX) + 1, 0:int(ny * startY)+1]), cmap='jet')
colbar = fig.colorbar(Col)
colbar.set_label("Velocity (m/s)")

Q = ax.quiver(Xn, Yn, sim.uxUnitPlot[plotPlace, int(ny * startY):int(ny * endY) + 1, int(nx * startX):int(nx * endX) + 1],
              sim.uyUnitPlot[plotPlace, int(ny * startY):int(ny * endY) + 1, int(nx * startX):int(nx * endX) + 1], pivot='mid', color='black', units='width')

plt.title("Lid-driven cavity velocity solution using 21x21 nodes spacially scaled by 1.1")
plt.xlabel('x (m)')
plt.ylabel('y (m)')
ax_t = plt.axes([0.1, 0.01, 0.75, 0.03], facecolor='lightgoldenrodyellow')

t_slider = Slider(ax_t, 't (s)', sim.t[0], sim.t[-1], valinit=sim.t[0])


def get_index(arr, val):
    for i in range(len(arr)):
        if arr[i] >= val:
            return i

def update(val):
    t = t_slider.val
    index = get_index(sim.t, t)
    #
    Q.set_UVC(sim.uxUnitPlot[index, int(ny * startY):int(ny * endY) + 1, int(nx * startX):int(nx * endX) + 1],
              sim.uyUnitPlot[index, int(ny * startY):int(ny * endY) + 1, int(nx * startX):int(nx * endX) + 1])
    # Q.set_UVC(sim.uxUnitPlot[index, int(nx * startX):int(nx * endX) + 1, 0:int(ny * startY)+1],
    #           sim.uyUnitPlot[index, int(nx * startX):int(nx * endX) + 1, 0:int(ny * startY)+1])
    Col.set_array(sim.uMagPlot[index, int(ny * startY):int(ny * endY) + 1, int(nx * startX):int(nx * endX) + 1].flatten())
    # Col.set_array(sim.uMagPlot[index, int(nx * startX):int(nx * endX) + 1, 0:int(ny * startY)+1].flatten())

    fig.canvas.draw_idle()


t_slider.on_changed(update)

plt.show()