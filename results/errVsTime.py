from results.ReaderClasses import MeshReader as mr
from results.ReaderClasses import SimReader as sr
import matplotlib.pyplot as plt
import numpy as np

simName = "errCorr"

sim = sr(simName)

# plt.plot(sim.t[10:], np.log(sim.err[10:]))
plt.plot(sim.t[1:], sim.t[1:] - sim.t[:-1])
plt.show()