from results.ReaderClasses import MeshReader as mr
from results.ReaderClasses import SimReader as sr
import matplotlib.pyplot as plt
import numpy as np

simName = "errTest"

sim = sr(simName)

X, Y = np.meshgrid(sim.mesh.xNodesVerticies, sim.mesh.yNodesVerticies)
# Z = np.power(mesh.x, 2) + np.power(mesh.y, 2)
# Z = mesh.x
# Z = mesh.y
Z = np.power(sim.uy[-sim.nNodes:], 2) + np.power(sim.ux[-sim.nNodes:], 2)
Z = np.reshape(Z, [np.alen(sim.mesh.xNodes), np.alen(sim.mesh.yNodes)]).transpose()

plt.hlines(sim.mesh.yNodesVerticies, sim.mesh.xNodesVerticies[0], sim.mesh.xNodesVerticies[-1])
plt.vlines(sim.mesh.xNodesVerticies, sim.mesh.yNodesVerticies[0], sim.mesh.yNodesVerticies[-1])

plt.pcolor(X, Y, Z)
plt.colorbar()
# plt.grid()

plt.show()