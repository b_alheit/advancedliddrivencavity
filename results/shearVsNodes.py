from results.ReaderClasses import MeshReader as mr
from results.ReaderClasses import SimReader as sr
import matplotlib.pyplot as plt
import numpy as np

sim2110 = sr("21_1.0_10")
sim4110 = sr("41_1.0_10")
sim6110 = sr("61_1.0_10")
sim8110 = sr("81_1.0_10")

shear10 = np.array([sim2110.shear, sim4110.shear, sim6110.shear, sim8110.shear])
dx10 = np.array([sim2110.Nodes_neg1, sim4110.Nodes_neg1, sim6110.Nodes_neg1, sim8110.Nodes_neg1])
nNodes10 = np.array([sim2110.nNodes, sim4110.nNodes, sim6110.nNodes, sim8110.nNodes])

sim21 = sr("21_1.1_10")
sim41 = sr("41_1.1_10")
sim61 = sr("61_1.1_10")

shear = np.array([sim21.shear, sim41.shear, sim61.shear])
dx = np.array([sim21.Nodes_neg1, sim41.Nodes_neg1, sim61.Nodes_neg1])
nNodes = np.array([sim21.nNodes, sim41.nNodes, sim61.nNodes])


plt.plot(nNodes10, shear10, marker='o', label="Equispaced mesh")
plt.plot(nNodes, shear, marker='s', label="1.1 scaled mesh")

plt.legend()
plt.grid()
plt.xlabel("Total nodes")
plt.ylabel("Shear force on lid $(N)$")

plt.title("Shear force vs number of nodes")

plt.show()