\documentclass{article}

\author{Benjamin Alheit}
\date{\today}
\title{MEC5069Z Lid-driven cavity project}

\usepackage{graphicx}
\graphicspath{{figures/}}
\usepackage[margin = 1in]{geometry}
\usepackage{enumitem}
\usepackage{subcaption}
\usepackage{amsmath} 
\usepackage{amssymb} 
\usepackage{bm}
\usepackage{float}
\usepackage{subfig}
\usepackage[backend=biber]{biblatex}
%\bibliography{ref.bib}
%\usepackage[nottoc]{tocbibind}


\begin{document}
\maketitle
\subsection*{Introduction}
\noindent The purpose of this project was to simulate a lid-driven cavity at moderate Reynold's numbers to obtain the analytical value of the total shear force on the lid. In doing so, the presence of recirculation eddies were also observed in the bottom corners of the cavity and each simulation's ability to model these eddies is discussed.

The parameters of the simulations are as follows:

\begin{equation}
\begin{gathered}
\rho = 1 kg/m^3, \\
\mu = 0.01kg/ms, \\
l = 1m,\\
u_{lid} = 10m/s, \\
Re = \frac{\rho l u_{lid}}{\mu} = 1000.
\end{gathered}
\end{equation}
Where $\rho$ is the density of the fluid, $\mu$ is the viscosity of the fluid, $l$ is the length of the sides of the cavity, $u_{lid}$ is the velocity of the lid and $Re$ is Reynold's number. The problem was simulated using $21\times21$ nodes, $41\times41$ nodes, $61\times61$ nodes and $81\times81$ nodes on an equispaced mesh and using $21\times21$ nodes, $41\times41$ nodes and $61\times61$ nodes on a mesh where the spacing between the nodes was scaled by 1.1 for each adjacent cell. A higher-order CUI ($\kappa = \frac{1}{3}$) upwinding scheme was for all simulations. 
\subsection*{Ability to model eddies}
The velocity solutions to the equispaced mesh simulations and scaled mesh simulations can be seen in figures \ref{fig:equispaced} and \ref{fig:scaled} respectively. At a high level qualitative view, the equispaced mesh and scaled mesh produce similar results. The most evident difference in the results produced by the equispaced and scaled meshes comes when the eddy in the bottom right corner is observed more closely. 

Zoomed in views of the bottom right corner for the equispaced and scaled mesh solutions can be seen in figures \ref{fig:equispacedC} and \ref{fig:scaledC} respectively. Figure \ref{fig:equispacedC21} shows that the most coarse equispaced mesh does not display any evidence of a recirculation eddy in the bottom right corner, whereas figure \ref{fig:scaledC21} shows that the scaled mesh can capture the presence of an eddy in the bottom right corner with a $21\times21$ node mesh. In a similar fashion, using a scaled mesh of $61\times61$ nodes shows presence of a second recirculation eddy as shown clearly in figure \ref{fig:scaledC61zoom}, whereas an equispaced mesh of $81\times81$ nodes still does not capture this eddy as can be seen in figure \ref{fig:equispacedC81}. 

It is clear that a scaled mesh can model the presence of eddies in the corners of the cavity far better than an equispaced mesh using the same number of nodes. This is to be expected since the scaled clusters more nodes where the eddies will occur.

\subsection*{Mesh refinement study}
Since a CUI upwinding scheme was used the leading error term should be $O(\Delta x^3)$. Hence, if the mesh is fine enough such that the nondominant error terms are negligible, any metric of the simulation should be linearly proportional to $\Delta x^3$. The fact that it is proportional allows one to use an equivalent average $\Delta x$ value, 
\begin{equation}
\Delta x \propto (1/N_{nodes})^{(1/2)},
\end{equation}
where $N_{nodes}$ is the total number of nodes used in the simulation. The value is to the power of $1/2$ since it is a 2-dimensional simulation. Therefore, an equivalent value for $\Delta x^3$ can be determined by 
\begin{equation}
\Delta x^3 \propto (1/N_{nodes})^{(3/2)}.
\end{equation}
Hence, in theory, plotting some metric, such as the shear force on the lid of the cavity, against $(1/N_{nodes})^{(3/2)}$ should give a straight line. However, as can be seen in figure \ref{fig:mrs32} this is not the case. Furthermore it was observed that when the shear was plotted against $(1/N_{nodes})^{(1/6)}$ (note that $\frac{1}{dimension}\frac{1}{order} = \frac{1}{6}$) a straight line was produced as shown in figure \ref{fig:mrs16}. However, if one were to extrapolate these lines to a $\Delta x$ of $0$ different results would be obtained for the shear on the lid predicted by a simulation using an equispaced mesh and a simulation using a scaled mesh, which obviously is not correct. Figure \ref{fig:tvn} was created to see if the total shear force on the lid was tending to a constant value for an increasing number of nodes. The figure shows the classic curve of a metric tending to a particular value for an increasing mesh size, however the value that it is tending to is again different for the equispaced and scaled simulations. Furthermore, from a qualitative point of view, neither set of simulations is close to reaching the value of the analytical solution since neither curve in figure \ref{fig:tvn} is close to approaching a gradient of 0.

It is abundantly clear that something has gone wrong in the mesh refinement study. The possible errors (in approximate order of likelihood) are:
\begin{enumerate}
\item A mistake has been made in the procedure of the mesh refinement study.
\item The mesh resolution is not fine enough yet to extract any useful data. If the mesh were finer the shear force would be linearly correlated to $(1/N_{nodes})^{(3/2)}$ and the shear force predicted by both the equispaced simulations and the scaled mesh simulations would tend to the same value.
\item A mistake has been made in the simulation procedure and the solutions produced by the simulations are incorrect.
\end{enumerate}
\begin{figure}[H]
 \centering
\begin{subfigure}{0.48\textwidth}
\includegraphics[width=\linewidth]{21_10} 
\label{fig:equispaced21}
\caption{ }
\end{subfigure}
\begin{subfigure}{0.48\textwidth}
\includegraphics[width=\linewidth]{41_10}
\label{fig:equispaced41}
\caption{ }
\end{subfigure}

\begin{subfigure}{0.48\textwidth}
\includegraphics[width=\linewidth]{61_10}
\caption{}
\label{fig:equispaced61}
\end{subfigure}
\begin{subfigure}{0.48\textwidth}
\includegraphics[width=\linewidth]{81_10}
\caption{}
\label{fig:equispaced81}
\end{subfigure}

\caption{Plot of equispaced mesh velocity solutions for meshes of a) $21\times21$ nodes, b) $41\times41$ nodes, c) $41\times41$ nodes and d) $81\times81$ nodes}
\label{fig:equispaced}
\end{figure}
\pagebreak

\begin{figure}[H]
 \centering
\begin{subfigure}{0.48\textwidth}
\includegraphics[width=\linewidth]{21_10c} 
\caption{ }
\label{fig:equispacedC21}
\end{subfigure}
\begin{subfigure}{0.48\textwidth}
\includegraphics[width=\linewidth]{41_10c}
\caption{ }
\label{fig:equispacedC41}
\end{subfigure}

\begin{subfigure}{0.48\textwidth}
\includegraphics[width=\linewidth]{61_10c}
\caption{}
\label{fig:equispacedC61}
\end{subfigure}
\begin{subfigure}{0.48\textwidth}
\includegraphics[width=\linewidth]{81_10c}
\caption{}
\label{fig:equispacedC81}
\end{subfigure}

\caption{Zoom in of bottom right corner of equispaced mesh velocity solutions for meshes of a) $21\times21$ nodes, b) $41\times41$ nodes, c) $41\times41$ nodes and d) $81\times81$ nodes}
\label{fig:equispacedC}
\end{figure}
\pagebreak


\begin{figure}[H]
 \centering
\begin{subfigure}{0.48\textwidth}
\includegraphics[width=\linewidth]{21_11} 
\label{fig:scaled21}
\caption{ }
\end{subfigure}
\begin{subfigure}{0.48\textwidth}
\includegraphics[width=\linewidth]{41_11}
\label{fig:scaled41}
\caption{ }
\end{subfigure}

\begin{subfigure}{0.96\textwidth}
\includegraphics[width=\linewidth]{61_11}
\caption{}
\label{fig:scaled61}
\end{subfigure}


\caption[Caption for LOF]{Plot of scaled mesh velocity solutions for meshes of a) $21\times21$ nodes, b) $41\times41$ nodes and c) $61\times61$ nodes. The velocity direction arrows have been excluded from c) as they are too small to see and they obscure the velocity magnitude in the corners.}
\label{fig:scaled}
\end{figure}

\pagebreak


\begin{figure}[H]
 \centering
\begin{subfigure}{0.48\textwidth}
\includegraphics[width=\linewidth]{21_11c} 
\caption{ }
\label{fig:scaledC21}
\end{subfigure}
\begin{subfigure}{0.48\textwidth}
\includegraphics[width=\linewidth]{41_11c}
\caption{ }
\label{fig:scaledC41}
\end{subfigure}

\begin{subfigure}{0.48\textwidth}
\includegraphics[width=\linewidth]{61_11c}
\caption{}
\label{fig:scaledC61}
\end{subfigure}
\begin{subfigure}{0.48\textwidth}
\includegraphics[width=\linewidth]{61_11cZoom}
\caption{}
\label{fig:scaledC61zoom}
\end{subfigure}

\caption{Zoom in of bottom right corner of scaled mesh velocity solutions for meshes of a) $21\times21$ nodes, b) $41\times41$ nodes and c) $61\times61$ nodes. d) Is another view of c) where the second recirculation eddy has been zoomed in on.}
\label{fig:scaledC}
\end{figure}

\pagebreak


\begin{figure}[H]
 \centering
\begin{subfigure}{0.5\textwidth}
\includegraphics[width=\linewidth]{tvn} 
\caption{ }
\label{fig:tvn}
\end{subfigure}

\begin{subfigure}{0.5\textwidth}
\includegraphics[width=\linewidth]{mrs32}
\caption{ }
\label{fig:mrs32}
\end{subfigure}

\begin{subfigure}{0.5\textwidth}
\includegraphics[width=\linewidth]{mrs16}
\caption{}
\label{fig:mrs16}
\end{subfigure}


\caption{Plots generated in mesh refinement study. a) Total shear force on lid vs number of nodes. b) Attempt to find shear force as a function of the average node spacing and order of upwinding scheme. c) Plot of observed linear correlation between the shear force and (1/Total nodes)$^{(1/6)}$}
\label{fig:mrs}
\end{figure}


\printbibliography
\end{document}
